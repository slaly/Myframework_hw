<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table(name="person", indexes={@ORM\Index(name="IDX_34DCD176DD842E46", columns={"position_id"})})
 * @ORM\Entity
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="person_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prename", type="string", length=50, nullable=true)
     */
    private $prename;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fname", type="string", length=50, nullable=true)
     */
    private $fname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="lname", type="string", length=50, nullable=true)
     */
    private $lname;

    /**
     * @var \Position
     *
     * @ORM\ManyToOne(targetEntity="Position")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="position_id", referencedColumnName="position_id")
     * })
     */
    private $position;



    /**
     * Get personId.
     *
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set prename.
     *
     * @param string|null $prename
     *
     * @return Person
     */
    public function setPrename($prename = null)
    {
        $this->prename = $prename;

        return $this;
    }

    /**
     * Get prename.
     *
     * @return string|null
     */
    public function getPrename()
    {
        return $this->prename;
    }

    /**
     * Set fname.
     *
     * @param string|null $fname
     *
     * @return Person
     */
    public function setFname($fname = null)
    {
        $this->fname = $fname;

        return $this;
    }

    /**
     * Get fname.
     *
     * @return string|null
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set lname.
     *
     * @param string|null $lname
     *
     * @return Person
     */
    public function setLname($lname = null)
    {
        $this->lname = $lname;

        return $this;
    }

    /**
     * Get lname.
     *
     * @return string|null
     */
    public function getLname()
    {
        return $this->lname;
    }

    /**
     * Set position.
     *
     * @param \Position|null $position
     *
     * @return Person
     */
    public function setPosition(\Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return \Position|null
     */
    public function getPosition()
    {
        return $this->position;
    }
}

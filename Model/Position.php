<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Position
 *
 * @ORM\Table(name="position")
 * @ORM\Entity
 */
class Position
{
    /**
     * @var int
     *
     * @ORM\Column(name="position_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $positionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="position_name", type="string", length=50, nullable=true)
     */
    private $positionName;



    /**
     * Get positionId.
     *
     * @return int
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * Set positionName.
     *
     * @param string|null $positionName
     *
     * @return Position
     */
    public function setPositionName($positionName = null)
    {
        $this->positionName = $positionName;

        return $this;
    }

    /**
     * Get positionName.
     *
     * @return string|null
     */
    public function getPositionName()
    {
        return $this->positionName;
    }
}

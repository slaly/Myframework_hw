<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * PesrsonAddress
 *
 * @ORM\Table(name="pesrson_address", indexes={@ORM\Index(name="IDX_D9175782217BBB47", columns={"person_id"})})
 * @ORM\Entity
 */
class PesrsonAddress
{
    /**
     * @var string
     *
     * @ORM\Column(name="addr_type", type="string", length=50, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $addrType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="addr", type="text", length=16, nullable=true)
     */
    private $addr;

    /**
     * @var \Person
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="person_id")
     * })
     */
    private $person;



    /**
     * Set addrType.
     *
     * @param string $addrType
     *
     * @return PesrsonAddress
     */
    public function setAddrType($addrType)
    {
        $this->addrType = $addrType;

        return $this;
    }

    /**
     * Get addrType.
     *
     * @return string
     */
    public function getAddrType()
    {
        return $this->addrType;
    }

    /**
     * Set addr.
     *
     * @param string|null $addr
     *
     * @return PesrsonAddress
     */
    public function setAddr($addr = null)
    {
        $this->addr = $addr;

        return $this;
    }

    /**
     * Get addr.
     *
     * @return string|null
     */
    public function getAddr()
    {
        return $this->addr;
    }

    /**
     * Set person.
     *
     * @param \Person $person
     *
     * @return PesrsonAddress
     */
    public function setPerson(\Person $person)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person.
     *
     * @return \Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}

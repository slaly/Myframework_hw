<?php $__env->startSection('content'); ?>
   <h1><center>PERSON PAGE</center></h1>

   
   <table id ="tbPerson" class="table table-striped table-bordered" >
        <thead >

            <td> <button class="btn btn-outline-success"id = "add"onclick="location.href = 'http://localhost/myframework/addperson';">ADD</button>
            </td>
            <tr>
                <td>Person ID</td>
                <td>Position ID</td>
                <td>PreName</td>
                <td>FirstName</td>
                <td>LastName</td>
                <td>Manage</td>
            
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

<script>
    $(document).ready(function () {
        $('#tbPerson').DataTable({
            
            "ajax" : 'getdata',
          
          "columns":[
              {"data" : "person_id","className": "dt-center"},
              {"data"  : "position_id","className": "dt-center"},
              {"data"  : "prename"},
              {"data" :  "fname"},
              {"data"  : "lname"},
              {"data" : "per_id","className": "dt-center", render : function(data){
                  return '<button  class="btn btn-outline-warning" id ="edit" >EDIT!</button>' + "  " +'<button  class="btn btn-outline-danger" id = "delect" >DELETE!</button>';
              }
          }],
            responsive: true,
            fixedHeader: true
        });
      
    });
    
 $('#add').click(function (e) { 
     e.preventDefault();
    location.href = 'addperson';
 });






</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
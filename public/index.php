<?php
require '../vendor/autoload.php';  //เรียกใช้ เพื่อให้โหลดข้อมูลอัตโนมัต
$router = new AltoRouter();  // เรียกใช้ class ใน altoRouter.php
$router -> setBasePath('/myframework');  //base path 
$router -> map('GET','/person','','person');
$router -> map('GET','/editperson','','editperson');
$router -> map('GET','/addperson','','addperson');
$router -> map('GET','/deleteperson','','deleteperson');
$router -> map('GET','/getdata','','getdata');


$match = $router->match();
//var_dump($match);
//exit();
if($match){
    if($match['name']=="person"){
        require_once('../Controllers/PersonController.php');  
        $person = new PersonController();  //เรียกใช้ class
        $person->index(); //เรียกใช้ฟังก์ชั่น ใน class  ตามที่ระบุ path
    }else if($match['name']=="getdata"){
        require_once('../Controllers/PersonController.php'); 
        $person = new PersonController();
        $person->getData();
    
    }else if ($match['name']=="editperson") {
        require_once('../Controllers/PersonController.php');
        $person_edit = new PersonController();
        $person_edit->editPerson();
    }else if ($match['name']=="addperson") {
        require_once('../Controllers/PersonController.php');
        $person_add = new PersonController();
        $person_add->addPerson();
    }else if ($match['name']=="deleteperson") {
        require_once('../Controllers/PersonController.php');
        $person_delete = new PersonController();
        $person_delete->deletePerson();
    }
    
}else {
    header($_SERVER['SERVER_PROTOCOL'] . '404 Not Found');
    echo "ไม่พบ Page";
    
}

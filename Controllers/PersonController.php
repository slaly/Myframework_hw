<?php 
//require_once ('bootstrap.php');
use Philo\Blade\Blade;
class PersonController {
    public function index(){
       View('person');  //view จาก function
    }
    public function getData(){
        require_once "../bootstrap.php";

        $query = $entityManager->createQuery('SELECT p.personId,p.prename,p.fname,p.lname FROM Person p ');
        $result = $query->getResult();
        
         for ($i=0 ; $i<count($result); $i++) {
            $person[] = array (
                'person_id' => $result[$i]['personId'],
                'position_id' => $result[$i]['personId'],
                'prename' => $result[$i]['prename'],
                'fname' => $result[$i]['fname'],
                'lname' => $result[$i]['lname'],
                'per_id'=> $result[$i]['personId']
            );
         }
         $alldata = array (
            'data' => $person
        );
        echo json_encode($alldata);
    }
    public function editPerson(){
        View('person-edit');
    }
    public function addPerson(){
        View('person-add');
    }
    public function deletePerson(){
        View('person-delete');
    }
    
}
